@extends('backend.layouts.master')

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.dashboard.blog') }}</small>
</h1>
@endsection

@section('content')

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Create New Blog Post!</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box tools -->
    </div>
    <div class="box-body">
        <form action="{{ route('admin.blog.post.update') }}" method="post" role="form">
            <div class="container">
                <div class="form-group">
                    <label for="title">Title : </label>
                    <input type="text" class="form-control" name="title" id="title" {{ $errors->has('title') ? 'class=has-error' : '' }} value="{{ Request::old('title') ? Request::old('title') : isset($blog) ? $blog->title : '' }}" />
                </div>
                <div class="form-group">
                    <label for="author">Author :</label>
                    <input type="text" class="form-control" name="author" id="author" {{ $errors->has('author') ? 'class=has-error' : '' }} value="{{ Request::old('author') ? Request::old('author') : isset($blog) ? $blog->author : '' }}" />
                </div>
                <div class="form-group">
                    <label for="category_select">Add Category</label>
                    <select name="category_select" id="category_select">
                        <!-- foreach loop through categories -->
                        <option value="Dummy ID">Dummy  </option>
                    </select>&nbsp;
                    <button type="button" class="btn">Add Category</button>
                    <hr>
                    <div class="added-categories">
                        <ul>

                        </ul>
                    </div>
                    <input type="hidden" name="categories" id="categories">
                </div>

                <div class="input-group">
                  <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                      <i class="fa fa-picture-o"></i> Choose Thumbnail image
                  </a>
              </span>
              <input id="thumbnail" class="form-control" type="text" name="thumbnail" {{ $errors->has('thumbnail') ? 'class=has-error' : '' }} value="{{ Request::old('thumbnail') ? Request::old('thumbnail') : isset($blog) ? $blog->thumbnail : '' }}">
          </div>
          <img id="holder" style="margin-top:15px;max-height:100px;">


          <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" class="form-control" id="body" rows="12" cols="100" {{ $errors->has('body') ? 'class=has-error' : '' }} >{{ Request::old('body') ? Request::old('body') : isset($blog) ? $blog->body : '' }}</textarea>
        </div>
        <button type="submit" class="btn">Edit blog post</button>
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <input type="hidden" name="blog_id" value="{{ $blog->id }}">
    </div>

</form>
</div>
</div>



@endsection