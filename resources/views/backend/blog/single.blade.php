@extends('backend.layouts.master')

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@include('includes.partials.messages')

<section>
    <a href="{{ route('admin.blog.post.edit', ['blog_id' =>$blog->id]) }}" class="btn btn-info">Edit Blog Post</a>
    <a href="" class="btn btn-warning">Delete Blog Post</a>
</section>
<br>

    <div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $blog->title }}</h3>&nbsp;
        <div class="btn-group" role="group">
        </div>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <ul>
        <article>
            <div class="post-info">
                <h3>{{ $blog->title }}</h3>  
                <span class="info">{{ $blog->author->name }} | {{ $blog->created_at }}</span>    
                <p>{!! $blog->body !!}</p>                    
            </div>
        </article>
    </ul>
</div>

@endsection
