@extends('backend.layouts.master')

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
@include('includes.partials.messages')
<section>
    <a href="{{ route('admin.blog.create_post') }}" class="btn btn-info">New Blog Post</a>
</section>
<br>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Blog Updates </h3>&nbsp;
        <div class="btn-group" role="group">
            <a href="{{ route('admin.blog.post', ['blog_id' => $blog->id, 'end' => 'backend']) }}" class="btn btn-info">View Post</a>
            <a href="{{ route('admin.blog.post.edit', ['blog_id' =>$blog->id]) }}" class="btn btn-info">Edit Post</a>
            <a href="" class="btn btn-danger">Delete Post</a>
        </div>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <ul>
        @if(count($blogs) == 0)
        <li>No new Blogs</li>
        @else
        @foreach ($blogs as $blog)
        <article>
            <div class="post-info">
                <h3>{{ $blog->title }}</h3>  
                <span class="info">{{ $blog->author }} | {{ $blog->created_at }}</span>                        
            </div>
        </article>
        @endforeach
        @endif
    </ul>
</div>

@if($blogs->lastpage() > 1)
  <ul class="pagination">
    @if($blogs->currentPage() !== 1)
    <a href="{{ $blogs->previousPageUrl() }}"><i class="fa fa-caret-left"></i> 
      @endif
      @if($blogs->currentPage() !== $blogs->lastPage())
      <a href="{{ $blogs->nextPageUrl() }}"><i class="fa fa-caret-right"></i>
        @endif
      </ul>
      @endif

@endsection