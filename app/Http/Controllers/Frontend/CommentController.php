<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog\Comments;

use App\Http\Requests;

/**
 * Class CommentController
 * @package App\Http\Controllers
 */
class CommentController extends Controller
{
    public function store(Request $request)
    {
        $input['from_user'] = $request->user()->id;
        $input['on_blog'] = $request->input('on_blog');
        $input['body'] = $request->input('body');
        $slug = $request->input('slug');
        Comments::create( $input );
        return redirect()->route('blog.single', ['slug' => $slug])->with(['flash_success', 'Comment published']); 
    }
}
