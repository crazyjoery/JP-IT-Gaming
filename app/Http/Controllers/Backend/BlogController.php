<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog\Blog;
use App\Models\Blog\Category;

/**
 * Class BackendBlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{    

    public function getCreateBlog()
    {
        return view('backend.blog.create');
    }

    public function getBlogIndex()
    {
        $blogs = Blog::paginate(5);
        return view('backend.blog.index', compact('blogs'));
    }

    public function getSingleBlog($slug, $end = 'frontend')
    {
        $blog = Blog::where('slug', $slug)->first();
        if(!$blog) {
            return redirect()->route('blog.index')->with(['flash_warning' => 'Blog post not found!']);
        }
        return view($end . '.blog.single', ['blog' => $blog]);
    }

    public function getUpdateBlog(Request $request, $slug)
    {
        $blog = Blog::where('slug',$slug)->first();
        if(!$blog && ($request->user()->id == $blog->author_id))
        {
            return view('backend.blog.edit', ['blog' => $blog]);
            return redirect()->route('blog.index')->with(['flash_warning' => 'Blog post not found!']);
        }
    }

    public function postUpdateBlog(Request $request)
    {
        $blog_id = $request->input('blog_id');
        $blog = Blog::find('blog_id');
        if($blog && ($request->user()->id == $blog->author_id))
        {
            $title = $request['title'];
            $slug = str_slug($title);
            $duplicate = Blog::where('slug', $slug)->first();
            if ($duplicate)
            {
                if($duplicate->id != $blog_id)
                {
                    return redirect()->route('admin.blog.post.edit')->with(['flash_warning' => 'Title already exists!']);
                }
                else
                {
                    $blog->slug = $slug;
                }
            }
            $blog->title = $title;
            $blog->body = $request['body'];
            if ($request->has('save'))
            {
                $blog->active = 0;
                $message = 'Post Saved succesfully!';
                $landing = 'admin.blog.post.edit';
            }
            else {
                $blog->active = 1;
                $message = 'Post updated succesfully!';
                $landing = '$blog->slug';
            }
            $blog->save();
            return redirect()->route('$landing')->with(['flash_success' => '$message']);
        }
        else
        {
            return redirect()->route('admin.dashboard')->with(['flash_warning' => 'You are not allowed to do that!']);
        }

        // Categories
    }

    public function postCreateBlog(Request $request)
    {
        $this->validate($request, [
                        'title' => 'required|max:120|unique:blogs',
                        'title' => array('Regex:/^[A-Za-z0-9 ]+$/'),
                        'body' => 'required'
                        ]);

        $blog = new Blog();
        $blog->title = $request['title'];
        $blog->body = $request['body'];
        $blog->slug = str_slug($blog->title);
        $blog->author_id = $request->user()->id;
        $blog->thumbnail = $request['thumbnail'];
        if($request->has('save'))
        {
            $blog->active = 0;
            $message = 'Post saved succesfully';
        }
        else
        {
            $blog->active = 1;
            $message = 'Post published succesfully';
        }
        $blog->save();

        // Attaching Categories

        return redirect()->route('admin.dashboard')->with(['flash_success' => $message]);
    }

}