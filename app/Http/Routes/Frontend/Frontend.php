<?php

/**
 * Frontend Controllers
 */
Route::get('/', 'FrontendController@index')->name('frontend.index');
Route::get('macros', 'FrontendController@macros')->name('frontend.macros');
Route::get('blog', ['uses' => 'BlogController@getBlogIndex', 'as' => 'blog.index']);
Route::get('blog/{slug}', ['uses' => 'BlogController@getSingleBlog', 'as' => 'blog.single']);
Route::post('comment/add',  ['uses' => 'CommentController@store', 'as' => 'blog.comment']);
Route::post('comment/delete/{id}', ['uses' => 'CommentController@destroy', 'as' => 'blog.comment.delete']);

/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User'], function() {
        Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
        Route::post('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
        Route::post('profile/avatar', ['uses' => 'ProfileController@updateAvatar', 'as' => 'frontend.user.profile.avatar']);
    });
});