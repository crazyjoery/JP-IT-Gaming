var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function(mix) {
 mix
     //.phpUnit()
     //.compressHtml()

    /**
     * Copy needed files from /node directories
     * to /public directory.
     */
     .copy(
           'bower_resources/font-awesome/css/font-awesome.min.css',
           'public/css/fonts/font-awesome'
           )
     .copy(
           'bower_resources/bootstrap/dist/css/bootstrap.min.css',
           'public/css'
           )
     .copy(
           'bower_resources/bootstrap/dist/js/bootstrap.min.js',
           'public/js/vendor/bootstrap'
           ).copy(
           'bower_resources/bootstrap/fonts',
           'public/fonts'
           )
     .copy(
           'bower_resources/jquery/dist/jquery.min.js',
           'public/js/vendor/jquery'
           )
     .copy(
           'bower_resources/font-awesome/fonts',
           'public/css/fonts/fonts')
     .copy(
           'bower_resources/jarallax/dist/jarallax.min.js',
           'resources/assets/js/plugin/jarallax'
           )
     .copy(
           'bower_resources/owl.carousel/dist/owl.carousel.min.js',
           'resources/assets/js/plugin/owl-carousel'
           )
     .copy(
           'bower_resources/smoothscroll-for-websites/SmoothScroll.js',
           'resources/assets/js/plugin/smoothscroll'
           )
     .copy(
           'bower_resources/HexagonProgress/jquery.hexagonprogress.min.js',
           'resources/assets/js/plugin/hexagonprogress'
           )
     .copy(
           'bower_resources/jquery.countdown/dist/jquery.countdown.min.js',
           'resources/assets/js/plugin/countdown'
           )
     .copy(
           'bower_resources/isotope/dist/isotope.pkgd.js',
           'resources/assets/js/plugin/isotope'
           )
     .copy(
           'bower_resources/social-likes/dist/social-likes.min.js',
           'public/js/vendor/social-likes'
           )
     .copy(
           'bower_resources/social-likes/dist/social-likes_flat.css',
           'public/css'
           )
     .copy(
           'bower_resources/magnific-popup/dist/jquery.magnific-popup.min.js',
           'public/js/vendor/magnific-popup'
           )
     .copy(
           'bower_resources/magnific-popup/dist/magnific-popup.css',
           'public/css'
           )

     /**
      * Process frontend SCSS stylesheets
      */
     .sass([
        'frontend/youplay.scss',
        'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/frontend/app.css')

     /**
      * Combine pre-processed frontend CSS files
      */
     .styles([
        'frontend/app.css'
     ], 'public/css/frontend.css')

     /**
      * Combine frontend scripts
      */
     .scripts([
        'plugin/sweetalert/sweetalert.min.js',
        'plugin/youplay/youplay.min.js',
        'plugin/jarallax/jarallax.js',
        'plugin/smoothscroll/SmoothScroll.js',
        'plugin/hexagonprogress/jquery.hexagonprogress.min.js',
        'plugin/countdown/jquery.countdown.min.js',
        'plugin/isotope/isotope.pkgd.js',
        'plugins.js',
        'frontend/app.js'
     ], 'public/js/frontend.js')

     /**
      * Process backend SCSS stylesheets
      */
     .sass([
         'backend/app.scss',
         'backend/plugin/toastr/toastr.scss',
         'plugin/sweetalert/sweetalert.scss'
     ], 'resources/assets/css/backend/app.css')

     /**
      * Combine pre-processed backend CSS files
      */
     .styles([
         'backend/app.css'
     ], 'public/css/backend.css')

     /**
      * Make RTL (Right To Left) CSS stylesheet for the backend
      */
     .rtlCss()

     /**
      * Combine backend scripts
      */
     .scripts([
         'plugin/sweetalert/sweetalert.min.js',
         'plugins.js',
         'backend/app.js',
         'backend/plugin/toastr/toastr.min.js',
         'backend/custom.js'
     ], 'public/js/backend.js')

     /**
      * Combine pre-processed rtl CSS files
      */
     .styles([
         'rtl/bootstrap-rtl.css'
     ], 'public/css/rtl.css')

    /**
      * Apply version control
      */
     .version([
         "public/css/frontend.css",
         "public/js/frontend.js",
         "public/css/backend.css",
         "public/css/backend-rtl.css",
         "public/js/backend.js",
         "public/css/rtl.css"
     ]);
});